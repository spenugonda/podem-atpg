# README ME #
ATPG PODEM algorithm based on that described in the book "VLSI test principles and architectures" by Wang, et.al

### What is this repository for? ###

* To generate an exhaustive set of input vectors for a strictly combinational IC
* To find fault location of a manufactured IC whose circuit design is already known.

### How do I get set up? ###

* Import Makefile and .c,.h and .l files into your linux machine with gcc installed
* Run the makefile and make sure your circuit description is in the same format as part1test/c5315.bench


### Contribution guidelines ###

* reduced runtime of main.cc
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner: sushalpenugonda@gmail.com