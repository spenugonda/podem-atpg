// ESE-549 Project 3 Handout
// Your name here!

// For you to do:
// Part 1: 
//    - Update the evalGate() function to work with five-valued logic
//    - Update the evalXORGate() function to work with five-valued logic
//    - Update the LogicNot() function to work with five-valued logic
//    - Write the setValueCheckFault() function
// Then your first "mode 0" 5-valued logic/fault simulator should be finished.
// Test it.
//
// Part 2: 
//    - Write the getObjective() function*
//    - Write the updateDFrontier() function*
//    - Write the backtrace() function
//    - Write the podemRecursion() function
// Then your "mode 1/2" basic PODEM implementation should be finished.
// Test this code carefully.
// Note you can also turn on a "checkTest" mode that will use your Part 1
// simulator to run the test after you generated it and check that it
// correctly detects the faults.
// (Note also that this doesn't make sense unless you have carefully
// checked your solution to Part 1.)
//
// Part 3:
//    - Write the eventDrivenSim() function.
//    - Change the simulation calls in your podemRecursion() function
//      from using the old simFullCircuit() function to the new
//      event-driven simulator.
// Then, your PODEM implementation should run considerably faster
// (probably 8 to 10x faster for large circuits).
// Test everything and then evaluate the speedup.
// A quick way to figure out how long it takes to run something in
// Linux is:
//   time ./atpg ... type other params...
// 
//
// Part 4:
//    - Here you have a number of options for things to try. Please
//      see the project assignment for some ideas and direction.


#include <iostream>
#include <fstream> 
#include <vector>
#include <queue>
#include <time.h>
#include <stdio.h>
#include "parse_bench.tab.h"
#include "ClassCircuit.h"
#include "ClassGate.h"
#include <limits>
#include <stdlib.h>
#include <time.h>

using namespace std;

// Just for the parser
extern "C" int yyparse();
extern FILE *yyin; // Input file for parser

// Our circuit. We declare this external so the parser can use it more easily.
extern Circuit* myCircuit;


// --------------------------------
// Helper functions
void printUsage();
vector<char> constructInputLine(string line);
bool checkTest(Circuit* myCircuit);
string printPIValue(char v);

// ------------------------------

//----------------------------
// Functions for logic simulation
void simFullCircuit(Circuit* myCircuit);
void simGateRecursive(Gate* g);
void eventDrivenSim(Circuit* myCircuit, queue<Gate*> q);
char simGate(Gate* g);
char evalGate(vector<char> in, int c, int i);
char EvalXORGate(vector<char> in, int inv);
int LogicNot(int logicVal);
void setValueCheckFault(Gate* g, char gateValue);
//-----------------------------

//----------------------------
// Functions for PODEM:
bool podemRecursion(Circuit* myCircuit);
bool getObjective(Gate* &g, char &v, Circuit* myCircuit);
void updateDFrontier(Circuit* myCircuit);
void printDFrontier();
void backtrace(Gate* &pi, char &piVal, Gate* objGate, char objVal, Circuit* myCircuit);
void getXInput(Gate* &i,char tempObj);
bool isDOutput(vector<int> POGateValues);
//--------------------------

///////////////////////////////////////////////////////////
// Global variables
// These are made global to make your life slightly easier.

// A vector of Gate pointers for storing the D-Frontier.
vector<Gate*> dFrontier;

// When the main function sets everything up, it will use these
// variables to store a pointer to the Gate that has the fault
// on its output, and to store the value you will need to 
// set to activate the fault. (E.g. if it's a stuck-at-1 fault on
// the output of a gate named A, then
//   - faultLocation is a pointer to Gate A
//   - faultActivationVal will be set to LOGIC_ZERO.
Gate* faultLocation;
char faultActivationVal;

///////////////////////////////////////////////////////////



// You do not need to change anything in the main function,
// although you should understand what is happening
// here.
int main(int argc, char* argv[]) {

	// Check the command line input and usage
	if ((argc != 4) && (argc != 5) && (argc != 6)) {
		printUsage();
		return 1;
	}

	// Check the mode
	int mode = atoi(argv[1]);
	if ((mode != 0) && (mode != 1) && (mode != 2)) {
		printUsage();
		return 1;
	}

	// Parse the bench file and initialize the circuit. (Using C style for our parser.)
	FILE *benchFile = fopen(argv[2], "r");
	if (benchFile == NULL) {
		cout << "ERROR: Cannot read file " << argv[2] << " for input" << endl;
		return 1;
	}
	yyin = benchFile;
	yyparse();
	fclose(benchFile);

	myCircuit->setupCircuit();
	cout << endl;

	// Setup the output text file
	ofstream outputStream;
	outputStream.open(argv[3]);
	if (!outputStream.is_open()) {
		cout << "ERROR: Cannot open file " << argv[3] << " for output" << endl;
		return 1;
	}

	// If we are in logic simulation mode, then we proceed as in Project 1 (with the
	// exception that now we have D and D' values, and a potential SSA fault somewhere.
	if (mode == 0) {
		ifstream faultStream;
		string faultLocStr;
		faultStream.open(argv[4]);
		if (!faultStream.is_open()) {
			cout << "ERROR: Cannot open fault file " << argv[4] << " for input" << endl;
			return 1;
		}
		bool anyLines = false;

		// For each line in our fault file...
		while (getline(faultStream, faultLocStr)) {

			// Clear the old fault
			myCircuit->clearFaults();

			string faultTypeStr;
			if (!(getline(faultStream, faultTypeStr))) {
				break;
			}

			char faultType = atoi(faultTypeStr.c_str());

			// If == -1, then we are asking for fault-free simulation
			if (faultLocStr != "-1") {
				faultLocation = myCircuit->findGateByName(faultLocStr);
				faultLocation->set_faultType(faultType);
			}

			// Open the input vector file 
			ifstream inputStream;
			string inputLine;
			if (mode == 0) {
				inputStream.open(argv[5]);
				if (!inputStream.is_open()) {
					cout << "ERROR: Cannot read file " << argv[5] << " for input" << endl;
					return 1;
				}

				outputStream << "--" << endl;

				// Try to read a line of inputs from the file.
				while (getline(inputStream, inputLine)) {

					// Clear logic values in my circuit and set new values
					myCircuit->clearGateValues();
					myCircuit->setPIValues(constructInputLine(inputLine));

					// Simulate the circuit
					simFullCircuit(myCircuit);
					//Optional: update and print D Frontier gates
					//updateDFrontier(myCircuit);
					//printDFrontier();
					//myCircuit->printAllGates();

					// For each test vector, print the outputs and then the faults detectable at each gate.
					vector<Gate*> outputGates = myCircuit->getPOGates();
					for (int i = 0; i < outputGates.size(); i++) {
						outputStream << outputGates[i]->printValue();
					}
					outputStream << endl;

				}
				inputStream.close();
			}
		}
		faultStream.close();
	}

	// Here, we are in PODEM mode.
	else if (mode == 1) {
		// Open the fault file.
		ifstream faultStream;
		string faultLocStr;
		faultStream.open(argv[4]);
		if (!faultStream.is_open()) {
			cout << "ERROR: Cannot open fault file " << argv[4] << " for input" << endl;
			return 1;
		}


		// For each line in our fault file...
		while (getline(faultStream, faultLocStr)) {

			// Clear the old fault
			myCircuit->clearFaults();

			string faultTypeStr;

			if (!(getline(faultStream, faultTypeStr))) {
				break;
			}

			char faultType = atoi(faultTypeStr.c_str());

			// If == -1, then we are asking for "fault-free."
			// This will always fail in ATPG (you can't find a test to detect no fault)
			// So, if faultLocStr == -1, then ATPG will run, fail to activate a fault
			// and report that no test could be found.
			if (faultLocStr != "-1") {
				faultLocation = myCircuit->findGateByName(faultLocStr);
				faultLocation->set_faultType(faultType);
				faultActivationVal = (faultType == FAULT_SA0) ? LOGIC_ONE : LOGIC_ZERO;
			}

			// set all gate values to X
			for (int i = 0; i < myCircuit->getNumberGates(); i++) {
				myCircuit->getGate(i)->setValue(LOGIC_X);
			}

			// initialize the D frontier.
			dFrontier.clear();

			// call recursive function
			bool res = podemRecursion(myCircuit);

			// If we succeed, print the test we found to the output file.
			if (res == true) {
				vector<Gate*> piGates = myCircuit->getPIGates();
				for (int i = 0; i < piGates.size(); i++)
					outputStream << printPIValue(piGates[i]->getValue());
				outputStream << endl;
			}

			// If we failed to find a test, print a message to the output file
			else {
				outputStream << "none found" << endl;
			}

			// Lastly, you can use this to test that your PODEM-generated test
			// correctly detects the already-set fault.
			// Of course, this assumes that your simulation code (part 1) is tested
			// and correct.
			// If you want to enable/disable this, please see the checkTest() function
			// below.
			// You don't use this code when you are evaluating the runtime of your
			// ATPG system.

			if (res == true) {
				if (!checkTest(myCircuit)) {
					cout << "ERROR: PODEM returned true, but generated test does not detect fault on PO." << endl;
					myCircuit->printAllGates();
					assert(false);
				}
			}


			// Just printing to screen to monitor progress
			cout << "Fault = " << faultLocation->get_outputName() << " / " << (int)(faultType) << endl;
		}
		faultStream.close();
	}

	// Mode 2: generate tests for all faults in circuit
	else if (mode == 2) {

		for (int ii = 0; ii<myCircuit->getNumberGates(); ii++) {

			// -------------------------------- sa0 -----------------
			// Clear the old fault
			myCircuit->clearFaults();
			faultLocation = myCircuit->getGate(ii);
			char faultType = FAULT_SA0;
			faultLocation->set_faultType(faultType);
			faultActivationVal = (faultType == FAULT_SA0) ? LOGIC_ONE : LOGIC_ZERO;

			// set all gate values to X
			for (int i = 0; i < myCircuit->getNumberGates(); i++) {
				myCircuit->getGate(i)->setValue(LOGIC_X);
			}

			// initialize the D frontier.
			dFrontier.clear();

			// call recursive function
			bool res = podemRecursion(myCircuit);

			// If we succeed, print the test we found to the output file.
			if (res == true) {
				vector<Gate*> piGates = myCircuit->getPIGates();
				for (int i = 0; i < piGates.size(); i++)
					outputStream << printPIValue(piGates[i]->getValue());
				outputStream << endl;
				cout << "Y Fault = " << faultLocation->get_outputName() << " / " << (int)(faultType) << endl;
			}

			// If we failed to find a test, print a message to the output file
			else {
				outputStream << "none found" << endl;
				cout << "N Fault = " << faultLocation->get_outputName() << " / " << (int)(faultType) << endl;
			}

			if (res == true) {
				if (!checkTest(myCircuit)) {
					cout << "ERROR: PODEM returned true, but generated test does not detect fault on PO." << endl;
					myCircuit->printAllGates();
					assert(false);
				}
			}


			// ---------------------- sa1 --------------
			// Clear the old fault and add the sa1 fault
			myCircuit->clearFaults();
			faultLocation = myCircuit->getGate(ii);
			faultType = FAULT_SA1;
			faultLocation->set_faultType(faultType);
			faultActivationVal = (faultType == FAULT_SA0) ? LOGIC_ONE : LOGIC_ZERO;

			// set all gate values to X
			for (int i = 0; i < myCircuit->getNumberGates(); i++) {
				myCircuit->getGate(i)->setValue(LOGIC_X);
			}

			// initialize the D frontier.
			dFrontier.clear();

			// call recursive function
			res = podemRecursion(myCircuit);

			// If we succeed, print the test we found to the output file.
			if (res == true) {
				vector<Gate*> piGates = myCircuit->getPIGates();
				for (int i = 0; i < piGates.size(); i++)
					outputStream << printPIValue(piGates[i]->getValue());
				outputStream << endl;
				cout << "Y Fault = " << faultLocation->get_outputName() << " / " << (int)(faultType) << endl;
			}

			// If we failed to find a test, print a message to the output file
			else {
				outputStream << "none found" << endl;
				cout << "N Fault = " << faultLocation->get_outputName() << " / " << (int)(faultType) << endl;
			}

			if (res == true) {
				if (!checkTest(myCircuit)) {
					cout << "ERROR: PODEM returned true, but generated test does not detect fault on PO." << endl;
					myCircuit->printAllGates();
					assert(false);
				}
			}

		}
	}

	// close the output and fault streams
	outputStream.close();


	return 0;
}


/////////////////////////////////////////////////////////////////////
// Functions in this section are helper functions.
// You should not need to change these, except if you want
// to enable the checkTest function (which will use your simulator
// to attempt to check the test vector computed by PODEM.)


// Print usage information (if user provides incorrect input).
// You don't need to touch this.
void printUsage() {
	cout << "Usage: ./atpg [mode] [bench_file] [output_loc] [fault_file] [input_vectors]" << endl << endl;
	cout << "   mode:          0 for fault-simulation; 1 for targeted ATPG mode; 2 to run ATPG mode" << endl;
	cout << "                  for all possible SSA faults" << endl;
	cout << "   bench_file:    the target circuit in .bench format" << endl;
	cout << "   output_loc:    location for output file" << endl;
	cout << "   fault_file:    faults to be considered (for mode == 0 and 1 only)" << endl;
	cout << "   input_vectors: list of input vectors to simulate (for mode = 0 only)" << endl;
	cout << endl;
	cout << "   In mode 0, the system will simulate each vector in input_vectors for each" << endl;
	cout << "   in fault_file. For fault free simulation, enter -1 for the fault location and type." << endl;
	cout << endl;
	cout << "   In mode 1, the system will generate a test pattern for each fault listed" << endl;
	cout << "   in fault_file. In this mode, the input_vectors value is ignored." << endl;
	cout << endl;
	cout << "   In mode 2, the system will generate a test pattern for each SSA fault." << endl;
	cout << "   In this mode, fault_file and input_vectors are ignored." << endl;
	cout << endl;
}

// Just used to parse in the values from the input file.
// You don't need to touch this.
vector<char> constructInputLine(string line) {

	vector<char> inputVals;

	for (int i = 0; i<line.size(); i++) {
		if (line[i] == '0')
			inputVals.push_back(LOGIC_ZERO);

		else if (line[i] == '1')
			inputVals.push_back(LOGIC_ONE);

		else if ((line[i] == 'X') || (line[i] == 'x')) {
			inputVals.push_back(LOGIC_X);
		}

		else {
			cout << "ERROR: Do not recognize character " << line[i] << " in line " << i + 1 << " of input vector file." << endl;
			assert(false);
			//inputVals.push_back(LOGIC_X);
		}
	}
	return inputVals;
}

// This function gets called after your PODEM algorithm finishes.
// If you enable this, it will clear the circuit's internal values,
// and re-simulate the vector PODEM found to test your result.

// This is helpful when you are developing and debugging, but will just
// slow things down once you know things are correct.

// This function of course assumes that your simulation code (part 1)
// is correct and tested, or else you cannot trust the output here.

bool checkTest(Circuit* myCircuit) {

	// To enable this function, just comment out return true; here,
	// and the function will run.
	// To disable this function, just leave "return true;" here and
	// the system will not do the extra test after generating the vector.
	return true;

	// If we are here, then we want to run the test.

	simFullCircuit(myCircuit);

	// look for D or D' on an output
	vector<Gate*> poGates = myCircuit->getPOGates();
	for (int i = 0; i<poGates.size(); i++) {
		char v = poGates[i]->getValue();
		if ((v == LOGIC_D) || (v == LOGIC_DBAR)) {
			return true;
		}
	}

	// If we didn't find D or D' on any PO, then our test was not successful.
	return false;

}

// This is just a helper function used when storing the final test you computed.
// You don't need to run or modify this.
string printPIValue(char v) {
	switch (v) {
	case LOGIC_ZERO: return "0";
	case LOGIC_ONE: return "1";
	case LOGIC_UNSET: return "U";
	case LOGIC_X: return "X";
	case LOGIC_D: return "1";
	case LOGIC_DBAR: return "0";
	}
	return "";
}

// end of helper functions
//////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////
// Start of functions for circuit simulation.


// Full-circuit simulation: set all non-PI gates to LOGIC_UNKNOWN
// and call the recursive simulate function on all PO gates.
// Don't change this function.
void simFullCircuit(Circuit* myCircuit) {
	for (int i = 0; i<myCircuit->getNumberGates(); i++) {
		Gate* g = myCircuit->getGate(i);
		if (g->get_gateType() != GATE_PI)
			g->setValue(LOGIC_UNSET);
	}
	vector<Gate*> circuitPOs = myCircuit->getPOGates();
	for (int i = 0; i < circuitPOs.size(); i++) {
		simGateRecursive(circuitPOs[i]);
	}
}



// Recursive function to find and set the value on Gate* g.
// This function calls simGate and setValueCheckFault. You will
// change those functions, but not this one.
// Don't change this function.
void simGateRecursive(Gate* g) {

	// If this gate has an already-set value, you are done.
	if (g->getValue() != LOGIC_UNSET)
		return;

	// Recursively call this function on this gate's predecessors to
	// ensure that their values are known.
	vector<Gate*> pred = g->get_gateInputs();
	for (int i = 0; i<pred.size(); i++) {
		simGateRecursive(pred[i]);
	}

	// Once we get to here, we are ready to calculate the gate's value.
	char gateValue = simGate(g);

	// After I have calculated this gate's value, check to see if a fault changes it and set.
	setValueCheckFault(g, gateValue);
}



// This function performs event-driven simulation.
// Please see the project handout for a description of what
// we are doing here and why.

// This function takes as input the Circuit* and a queue<Gate*>
// indicating the remaining gates that need to be evaluated.

// TODOP3: Write this code. 

void eventDrivenSim(Circuit* myCircuit, queue<Gate*> q) {

	// while the queue is not empty {
	//   g = front of the queue; pop the queue.
	//   store the old (previously-set) value of the gate
	//   run the simGate() function (see below) to get the new output value of g
	//   if the old output value != the new output value {
	//       set the value of the gate (using setValueCheckFault() function)
	//       push the fanout gates of g onto the queue
	//   }
	// }
	while (!q.empty()){
		Gate* g = q.front();
		q.pop();
		char oldValue = g->getValue();
		if (g->get_gateType() != GATE_PI){
			char gateValue = simGate(g);
			if (oldValue != gateValue){
				setValueCheckFault(g, gateValue);
				vector<Gate*> fanOutGates = g->get_gateOutputs();
				for (int i = 0; i < fanOutGates.size(); i++){
					q.push(fanOutGates[i]);
				}
			}
		}
		else{
			setValueCheckFault(g, oldValue);
			vector<Gate*> fanOutGates = g->get_gateOutputs();
			for (int i = 0; i < fanOutGates.size(); i++){
				q.push(fanOutGates[i]);
			}
		}
		
	}
}



// This is a gate simulation function -- it will simulate the gate g
// with its current input values and return the output value.
// This function does not deal with the fault. (That comes later.)
// Do not change this function (but you will need to change the functions
// this calls: evalGate, LogicNot, and EvalXORGate) to make them
// correctly do 5-valued simulation.
char simGate(Gate* g) {

	// For convenience, create a vector of the values of this
	// gate's inputs.
	vector<Gate*> pred = g->get_gateInputs();
	vector<char> inputVals;
	for (int i = 0; i<pred.size(); i++) {
		inputVals.push_back(pred[i]->getValue());
	}

	char gateType = g->get_gateType();
	char gateValue;
	// Now, set the value of this gate based on its logical function and its input values
	switch (gateType) {
	case GATE_NAND: { gateValue = evalGate(inputVals, 0, 1); break; }//
	case GATE_NOR: { gateValue = evalGate(inputVals, 1, 1); break; }//
	case GATE_AND: { gateValue = evalGate(inputVals, 0, 0); break; }//
	case GATE_OR: { gateValue = evalGate(inputVals, 1, 0); break; }//
	case GATE_BUFF: { gateValue = inputVals[0]; break; }//
	case GATE_NOT: { gateValue = LogicNot(inputVals[0]); break; }
	case GATE_XOR: { gateValue = EvalXORGate(inputVals, 0); break; }//
	case GATE_XNOR: { gateValue = EvalXORGate(inputVals, 1); break; }//
	case GATE_FANOUT: { gateValue = inputVals[0]; break; }//
	default: { cout << "ERROR: Do not know how to evaluate gate type " << gateType << endl; assert(false); }
	}

	return gateValue;
}


// Evaluate a gate value (for non-XOR gates).
//    vector<int> in: logic values of the gate's inputs
//    int c: the controlling value of this gate (e.g., 0 for AND, 1 for OR)
//    int i: 1 if this gate is inverting (e.g., NAND), 0 if not inverting (e.g., AND)

// TODOP1: In part 1, you must modify this function to work with five-valued logic.
char evalGate(vector<char> in, int c, int i) {

	// Are any of the inputs of this gate the controlling value?
	bool anyC = find(in.begin(), in.end(), c) != in.end();

	// Are any of the inputs of this gate unknown?
	bool anyUnknown = (find(in.begin(), in.end(), LOGIC_X) != in.end());

	//Are any of the inputs of this gate D 
	bool anyD = find(in.begin(), in.end(), LOGIC_D) != in.end();

	//Are any of the inputs of this gate D_BAR
	bool anyDBAR = find(in.begin(), in.end(), LOGIC_DBAR) != in.end();

	// if any input is c or we have both D and D', then return c^i
	if (anyC || (anyD && anyDBAR))
		return (i) ? LogicNot(c) : c;

	// else if any input is unknown, return unknown
	else if (anyUnknown)
		return LOGIC_X;
	//is any input a D or a DBAR
	else if (anyD){
		return ((i) ? LOGIC_DBAR : LOGIC_D);
	}
	else if (anyDBAR){
		return ((i) ? LOGIC_D : LOGIC_DBAR);
	}
	// else return ~(c^i)
	else
		return LogicNot((i) ? LogicNot(c) : c);
}


// Evaluate an XOR or XNOR gate
//    vector<int> in: logic values of the gate's inputs
//    int inv: 1 if this gate is inverting (XNOR), 0 if not inverting (XOR)

// TODOP1: In part 1, you must modify this function to work with five-valued logic.
char EvalXORGate(vector<char> in, int inv) {

	// if any unknowns, return unknown
	bool anyUnknown = (find(in.begin(), in.end(), LOGIC_X) != in.end());
	if (anyUnknown)
		return LOGIC_X;

	int XORVal;

	if ((in[0] == LOGIC_ZERO) && (in[1] == LOGIC_ONE))
		XORVal = LOGIC_ONE;
	else if ((in[0] == LOGIC_ONE) && (in[1] == LOGIC_ZERO))
		XORVal = LOGIC_ONE;
	else if ((in[0] == LOGIC_ZERO) && (in[1] == LOGIC_ZERO))
		XORVal = LOGIC_ZERO;
	else if ((in[0] == LOGIC_ONE) && (in[1] == LOGIC_ONE))
		XORVal = LOGIC_ZERO;
	else if (((in[0] == LOGIC_D) && (in[1] == LOGIC_ZERO)) || ((in[0] == LOGIC_ZERO) && (in[1] == LOGIC_D)))//D and D' territory..
		XORVal = LOGIC_D;
	else if (((in[0] == LOGIC_D) && (in[1] == LOGIC_ONE)) || ((in[0] == LOGIC_ONE) && (in[1] == LOGIC_D)))
		XORVal = LOGIC_DBAR;
	else if (((in[0] == LOGIC_DBAR) && (in[1] == LOGIC_ZERO)) || ((in[0] == LOGIC_ZERO) && (in[1] == LOGIC_DBAR)))
		XORVal = LOGIC_DBAR;
	else if (((in[0] == LOGIC_DBAR) && (in[1] == LOGIC_ONE)) || ((in[0] == LOGIC_ONE) && (in[1] == LOGIC_DBAR)))
		XORVal = LOGIC_D;
	else if (((in[0] == LOGIC_D) && (in[1] == LOGIC_DBAR)) || ((in[0] == LOGIC_DBAR) && (in[1] == LOGIC_D)))
		XORVal = LOGIC_ONE;
	else
		XORVal = LOGIC_ZERO;
	//else
	//cout << "I need to make EvalXORGate handle D and D' values." << endl;

	return (inv) ? LogicNot(XORVal) : XORVal;

}


// A quick function to do a logical NOT operation on the LOGIC_* macros
// TODOP1: In part 1, you must modify this function to work with five-valued logic.
int LogicNot(int logicVal) {
	if (logicVal == LOGIC_ONE)
		return LOGIC_ZERO;
	if (logicVal == LOGIC_ZERO)
		return LOGIC_ONE;
	if (logicVal == LOGIC_X)
		return LOGIC_X;
	if (logicVal == LOGIC_D)
		return LOGIC_DBAR;
	if (logicVal == LOGIC_DBAR)
		return LOGIC_D;

	cout << "ERROR: Do not know how to invert " << logicVal << " in LogicNot(int logicVal)" << endl;
	return LOGIC_UNSET;
}



// This goal of this function is to set the value of the Gate* g to
// the gateValue.
// However, the gate may potentially have a stuck-at output fault.
// If it does, then you may need to change the value to LOGIC_D or
// LOGIC_DBAR.

// TODOP1: In part 1, you must write this function to:
//   - check if there is a fault on Gate* g.
//   - if there is, check the fault type and the value of gateValue,
//     and determine which value the gate should output.
//        (e.g. if g has a sa0 output fault and gateValue == LOGIC_ONE,
//         then you want to set the gate's output to LOGIC_D).
//   - then you should use the setValue() function of Gate to set
//     the gate's output value to the one you just calculated.
void setValueCheckFault(Gate* g, char gateValue) {

	// NOTE: This line below is not sufficient. Need to first check
	// for and handle any fault on g.
	if (gateValue == 1 && g->get_faultType() == FAULT_SA0){
		g->setValue(LOGIC_D);
		//cout << "test";
	}
	else if (gateValue == 0 && g->get_faultType() == FAULT_SA1){
		g->setValue(LOGIC_DBAR);
		//cout << "test1";
	}
	else{
		g->setValue(gateValue);
		//cout << "test2";
	}
}

// End of functions for circuit simulation
////////////////////////////////////////////////////////////


/////////////////////////////////////////////////////////
// Begin functions for PODEM.


// The main PODEM recursion
// TODOP2: Write this, following pseudocode in class and the comments below.
bool podemRecursion(Circuit* myCircuit) {
	//myCircuit->printAllGates();
	// If D or D' is at an output, then return true
	if (isDOutput(myCircuit->getPOValues())){
		return true;
	}

	Gate* g;
	char v;

	// Call the getObjective function. Store the result in g and v.  
	// If getObjective fails, return false
	if (getObjective(g, v, myCircuit)==false){
		return false;
	}
	
	Gate* pi;
	char piVal;


	// Call the backtrace function. Store the result in pi and piVal.
	backtrace(pi, piVal, g, v, myCircuit);
	
	
	// Set the value of pi to piVal. Use your setValueCheckFault function (see above)
	// to make sure if there is a fault on the PI gate, it correctly gets set.
	pi->setValue(piVal);
	setValueCheckFault(pi, piVal);
	// Now, determine the implications of the input you set by simulating 
	// the circuit by calling simFullCircuit(myCircuit);
	// TODOP3: After you write the event-driven simulator for part 3, replace
	// this call to simFullCircuit with a call to launch your event-driven
	// simulator.
	//simFullCircuit(myCircuit);
	queue<Gate*> q;
	q.push(pi);
	eventDrivenSim(myCircuit, q);

	// Recursively call this function. If the recursive function succeeds
	// return true.
	if (podemRecursion(myCircuit)){
		return true;
	}
	// If the recursive call fails, set the opposite PI value, simulate, it and recurse.
	// If this recursive call succeeds, return true.
	else{
		if (piVal == LOGIC_ONE){
			piVal = LOGIC_ZERO;
		}
		else if (piVal == LOGIC_ZERO){
			piVal = LOGIC_ONE;
		}
		else if (piVal == LOGIC_D){
			piVal = LOGIC_DBAR;
		}
		else if (piVal == LOGIC_DBAR){
			piVal = LOGIC_D;
		}
		pi->setValue(piVal);
		//simFullCircuit(myCircuit);
		eventDrivenSim(myCircuit, q);
		if (podemRecursion(myCircuit))return true;
		
	}

	// If we get to here, neither pi=v nor pi = v' worked. So, set pi to value X and 
	// return false.
	piVal = LOGIC_X;
	pi->setValue(piVal);
	//simFullCircuit(myCircuit);
	eventDrivenSim(myCircuit, q);
	return false;
}



// Find the objective for myCircuit. The objective is stored in g, v.
// TODOP2: Write this function, based on the pseudocode from
// class or your textbook.
bool getObjective(Gate* &g, char &v, Circuit* myCircuit) {

	// First you will need to check if the fault is activated yet.
	// Note that in the setup above we set up a global variable
	// Gate* faultLocation which represents the gate with the stuck-at
	// fault on its output. Use that when you check if the fault is
	// excited.

	// Another note: if the fault is not excited but the fault 
	// location value is not X, then we have failed to activate 
	// the fault. In this case getObjective should fail and Return false.
	char faultTemp = faultLocation->getValue();
	if (faultTemp != LOGIC_D && faultTemp != LOGIC_DBAR && faultTemp==LOGIC_X){
		g = faultLocation;
		char faultType = faultLocation->get_faultType();
		if (faultType == FAULT_SA0){
			v = LOGIC_ONE;
		}
		else if (faultType == FAULT_SA1){
			v = LOGIC_ZERO;
		}
		return true;
	}
	
	// If the fault is already activated, then you will need to 
	// use the D-frontier to find an objective.
	else if(faultTemp == LOGIC_D || faultTemp == LOGIC_DBAR){
		// Before you use the D-frontier you should update it by running:
		updateDFrontier(myCircuit);
		printDFrontier();
		// This function should update the global D-frontier variable

		// If the D frontier is empty after update, then getObjective fails
		// and should return false.
		if (dFrontier.size() == 0)
			return false;
		// getObjective needs to choose a gate from the D-Frontier.
		// For part 2, just pick any gate. (It's easy to just pick
		// dFrontier[0].
		// Later, a possible optimization in Part 4 is to use the 
		// SCOAP observability metric to choose this carefully.
		// Lastly, set the values of g and v based on the
		// gate you chose from the D-Frontier.
		//Gate * d = dFrontier[0];
		int i,minobs=-1,minobsindex=-1;
		for (i = 0; i < dFrontier.size(); i++)
		{
			if (minobsindex == -1)
			{
				minobs = dFrontier[i]->getObs();
				minobsindex = i;
			}
			else if (dFrontier[i]->getObs() < minobs)
			{
				minobs = dFrontier[i]->getObs();
				minobsindex = i;
			}
		}
		Gate* d = dFrontier[minobsindex];
		vector<Gate*> temp = d->get_gateInputs();
		for (int i = 0; i <temp.size(); i++){
			if (temp[i]->getValue() == LOGIC_X){
				g = temp[i];
				break;
			}
		}
		string gateType = d->gateTypeName();
		if (gateType == "NAND" || gateType == "AND"){
			v = LOGIC_ONE;
			return true;
		}
		else {//if (gateType == NOR || gateType == OR){
			v = LOGIC_ZERO;
			return true;
		}

	}
	if (faultTemp != LOGIC_X){
		return false;
	}
}



// A very simple method to update the D frontier.
// TODOP2: Write this code based on the pseudocode below.
void updateDFrontier(Circuit* myCircuit) {

	// clear the dFrontier vector.
	dFrontier.clear();
	int gnum = (myCircuit->getNumberGates()) - 1;
	for (gnum; gnum >= 0; gnum--){
		int dnum = 0, dbarnum = 0, xnum = 0;
		Gate* g = myCircuit->getGate(gnum);
		if (g->getValue() == LOGIC_X){
			vector<Gate*> tempGateInputs = g->get_gateInputs();
			for (int j = 0; j < tempGateInputs.size(); j++){
				char tempVal = tempGateInputs[j]->getValue();
				if (tempVal == LOGIC_X)
					xnum++;
				else if (tempVal == LOGIC_D)
					dnum++;
				else if (tempVal == LOGIC_DBAR)
					dbarnum++;

			}
			if (xnum > 0){
				if (dnum > 0 && dbarnum == 0)
					dFrontier.push_back(g);
				else if (dbarnum > 0 && dnum == 0)
					dFrontier.push_back(g);
			}
		}
	}
	// for g = each gate in the circuit {
	//    if (g should be on the D frontier, based on its current
	//                   input and output values) {
	//       add it to the D-Frontier vector;
	//    }
	// }

}




// Backtrace: given objective objGate and objVal, then figure out which input (pi) to set 
// and which value (piVal) to set it to.
// TODOP2: Write this code based on the psuedocode from class.
void backtrace(Gate* &pi, char &piVal, Gate* objGate, char objVal, Circuit* myCircuit) {
	Gate* i = objGate;
	int num_inversions = 0;
	piVal = objVal;
	char tempObj = objVal;
	while (i->get_gateType() != GATE_PI){
		char gateType = i->get_gateType();
		if (
			gateType == GATE_NAND ||
			gateType == GATE_NOR ||
			gateType == GATE_XNOR ||
			gateType == GATE_NOT
			){
			num_inversions++;
		}
		//cout << "test" << endl;
	
		
		getXInput(i,tempObj);//THIS SETS I TO AN INPUT WHOSE VALUE IS X
		
		if (num_inversions % 2 == 1){
			if (tempObj == LOGIC_ONE){
				tempObj = LOGIC_ZERO;
			}
			else if (tempObj == LOGIC_ZERO){
				tempObj = LOGIC_ONE;
			}
			else if (tempObj == LOGIC_D){
				tempObj = LOGIC_DBAR;
			}
			else if (tempObj == LOGIC_DBAR){
				tempObj = LOGIC_D;
			}
		}
	}
	pi = i;
	if (num_inversions % 2 == 1){
		if (piVal == LOGIC_ONE){
			piVal = LOGIC_ZERO;
		}
		else if (piVal == LOGIC_ZERO){
			piVal = LOGIC_ONE;
		}
		else if (piVal == LOGIC_D){
			piVal = LOGIC_DBAR;
		}
		else if (piVal == LOGIC_DBAR){
			piVal = LOGIC_D;
		}
	}
	

}

////////////////////////////////////////////////////////////////////////////
// Please place any new functions you add here, between these two bars.

//Prints out the gates with details that belong in D-Frontier
void printDFrontier(){
	for (int i = 0; i < dFrontier.size(); i++){
		dFrontier[i]->printGateInfo();
	}
}


//THIS SETS I TO AN INPUT WHOSE VALUE IS X
void getXInput(Gate* &i,char tempObj){
	int minContr = -1;
	int minCindex = -1;

	vector<Gate*> tempGates = i->get_gateInputs();
	for (int j = 0; j < tempGates.size(); j++){
		if (tempGates[j]->getValue() == LOGIC_X){
			if (minCindex == -1){
				if (tempObj == LOGIC_ONE){
					minContr = tempGates[j]->getCC1();
					minCindex = j;
				}
				else if (tempObj == LOGIC_ZERO){
					minContr = tempGates[j]->getCC0();
					minCindex = j;
				}
			}
			else{
				int cc1 = tempGates[j]->getCC1();
				int cc0 = tempGates[j]->getCC0();
				if (tempObj == LOGIC_ONE && cc1<minContr){
					minContr =cc1;
					minCindex = j;
				}
				else if (tempObj == LOGIC_ZERO && cc0<minContr){
					minContr = cc0;
					minCindex = j;
				}
			}
			//i = tempGates[j];
			//return;
		}
	}
	i = tempGates[minCindex];
	return;
}

bool isDOutput(vector<int> POGateValues){
	for (int i = 0; i < POGateValues.size(); i++){
		//cout << POGateValues[i] << ",";
		if (POGateValues[i] == 4 || POGateValues[i] == 3){
			return true;
		}
	}
	//cout << endl;
	return false;
}
////////////////////////////////////////////////////////////////////////////


